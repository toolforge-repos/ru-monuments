<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title>Карточка объекта из ЕГРКН</title>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>
    <style type="text/css">
      TABLE {border-collapse: collapse;}
      TH, TD {padding: 4px;}
      FORM {margin: 0;}
      #mapid {height: 180px;}
    </style>
  </head>
  <body>
  <table width="70%" align="center" border style="border-collapse: collapse">
    <tr><td colspan=2 valign="middle"><form method="get"><label for="id">Введите номер памятника:</label> <input id="id" name="id" value="<?php echo htmlspecialchars(trim($_GET["id"])); ?>"> <input type="submit" value="Искать"></form></td></tr>
<?php
  $knid_arr = array();
  $egrokn_arr = array();
  $result = false;

  $ts_pw = posix_getpwuid(posix_getuid());
  $ts_mycnf = parse_ini_file($ts_pw['dir'] . "/replica.my.cnf");
  $ts_egrokn_apikey = file_get_contents($ts_pw['dir']."/egrokn-api.key");
  $dbh = new PDO('mysql:host=tools-db;dbname=s54977__ruheritage', $ts_mycnf['user'], $ts_mycnf['password']);
  unset($ts_mycnf, $ts_pw);

  $id=trim($_GET["id"]);
  $url = 'https://opendata.mkrf.ru/v2/egrkn/$?f={"data.general.regNumber":{"$eq":"'.$id.'"}}';
  $opts = [
    "http" => [
        "timeout" => 10,
//        "proxy" => "tcp://94.130.229.212:8080",
        "method" => "GET",
        "header" => "X-API-KEY: ".$ts_egrokn_apikey
        ],
    "ssl" => [
        "verify_peer" => false
    ]
  ];

  $context = stream_context_create($opts);
//  $result = file_get_contents ($url, false, $context);
  $res = json_decode($result);
  do {
    if ($result === false) {
//      echo '<tr><td colspan=2>Ошибка: API открытых данных Минкульта РФ в данный момент недоступен</td></tr>';

      $sth = $dbh->prepare('SELECT * FROM egrokn WHERE knid_new = ?');
      $sth->execute(array($id));
      if (!($row = $sth->fetch(PDO::FETCH_ASSOC))) {
        echo '<tr><td colspan=2>Ошибка: памятник с таким номером не найден</td></tr>';
        break;
      }
      echo "<tr><td colspan=2>Информация из копии ЕГРОКН, архивированной $row[last_update]</td></tr>";
      $res = (object)array('data'=>array(json_decode($row['json'])));

    } elseif ($res->status != 200 or !property_exists($res, 'total') or $res->total != 1) {
      echo '<tr><td colspan=2>Ошибка: памятник с таким номером не найден</td></tr>';
      break;
    }

    $result = '';

    $props = $res->data[0]->data->general;
    $res = '';
    echo '<tr><th colspan=2 align="center">'.$props->name."</th></tr>";
    if (empty($props->photo->url) or is_null($props->photo->url))
        echo '<tr><td colspan=2 align="center">Нет фотографии</td></tr>';
    else {
        echo "<script type='text/javascript'>
            function imgwin(doc,ratio) {
                var wd = 800; var ht = 600;
                if (ratio > 0) {
                    if (wd/ratio > ht) wd = ht*ratio;
                    if (ht*ratio > wd) ht = wd/ratio;
                }
                ht += 20;
                var mywin=window.open('','','width='+wd+',height='+ht+',location=no,toolbar=no,menubar=no');
                mywin.document.write('<html><body>{$props->photo->url}<img src=\\'{$props->photo->url}\\' width='+wd+'></body></html>');
                doc.body.style.cursor='default';
            }
            </script>";
        echo "<tr><td colspan=2 align='center'><a href='{$props->photo->url}'
            onclick=\"document.body.style.cursor='wait'; var img=new Image(); img.src='{$props->photo->url}'; img.onload=function(){imgwin(document,this.width/this.height);}; img.onerror=function(){imgwin(document,0);}; return false;\">Фотография</a></td></tr>";
    }
    echo '<tr><th align="left" width="20%">Регион расположения объекта</th><td id=region>'.$props->region->value."</td></tr>";
    $t='';
    if (isset($props->address->mapPosition->coordinates))
    {
        $lat=$props->address->mapPosition->coordinates[1];
        $long=$props->address->mapPosition->coordinates[0];
    }
    echo '<tr><th align="left">Адрес</th><td><span id=address>'.$props->address->fullAddress."</span></td></tr>";
    echo "<tr><th align='left'>Координаты</th><td id=coords>".(empty($lat) || empty($long) ? '&nbsp;' : 
        "<div style='margin:2pt;'><a href='https://tools.wmflabs.org/geohack/geohack.php?params={$lat}_N_{$long}_E_globe:earth&language=ru' title='Карты и инструменты GeoHack'>{$lat}, {$long}</a>
        <sup><small><a href='https://maps.yandex.ru/?ll={$long},{$lat}&spn=0.02,0.02&pt={$long},{$lat}&l=map' title='Это место на Яндекс.Картах'>(Я)</a></small></sup>
        <sup><small><a href='https://maps.google.com/maps?ll={$lat},{$long}&spn=0.02,0.02&t=m&q={$lat},{$long}' title='Это место на картах Google'>(G)</a></small></sup>
        <sup><small><a href='https://www.openstreetmap.org/?mlat={$lat}&mlon={$long}&zoom=13&layers=M' title='Это место на карте OpenStreetMap'>(O)</a></small></sup></div>
        <div id='mapid'></div>
        <script type='text/javascript'>
            var wvmap = L.map('mapid').setView([{$lat}, {$long}], 13);
            var tile = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
//            var tile = 'https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png';
            L.tileLayer(tile, {attribution: '&copy; <a href=\"https://www.openstreetmap.org/\">OpenStreetMap</a> contributors, <a href=\"https://creativecommons.org/licenses/by-sa/2.0/\">CC-BY-SA</a>'}).addTo(wvmap);
            var marker = L.marker([{$lat}, {$long}]).addTo(wvmap);
        </script>")."</td></tr>";
    echo '<tr><th align="left">Номер в реестре</th><td id=regnumber>'.$props->regNumber."</td></tr>";
    echo '<tr><th align="left">Учетный номер</th><td id=connumber>'.$props->conNumber."</td></tr>";
    echo '<tr><th align="left">ID в наборе открытых данных</th><td id=odid>'.$props->id."</td></tr>";
    if (isset($props->parentId))
    {
      $result = false;
      $complex = false;
      $url2 = 'http://opendata.mkrf.ru/v1/egrkn/$/' . $props->parentId; 
//      $result = file_get_contents ($url2);
      $res2 = json_decode($result);
      if ($result === false) {
        $sth2 = $dbh->prepare('SELECT * FROM egrokn WHERE nativeid = ?');
        $sth2->execute(array($props->parentId));
        if ($row = $sth2->fetch(PDO::FETCH_ASSOC)) {
          $res2 = json_decode($row['json']);
          $complex = $res2->data->general->regNumber;
          $complex_name = $res2->data->general->name;
        }
      } elseif ($res2->status == 200 & $res2->total == 1) {
        $complex = $res2->data[0]->data->general->regNumber;
        $complex_name = $res2->data[0]->data->general->name;
      }
      if ($complex !== false)
        echo '<tr><th align="left">Элемент ансамбля</th><td id=ensemble>'.$complex_name. " (<a href=get_info.php?id=$complex>". $complex ."</a>)</td></tr>";
    }
    echo '<tr><th align="left">Категория охраны</th><td id=categorytype>'.$props->categoryType->value."</td></tr>";
    $unesco=$props->unesco->id;
    $esp_val=$props->status->id;
    if ($unesco == 1 | $esp_val == 1)
    {
       echo '<tr><th align="left">Дополнительно</th><td>';
       if ($unesco == 1)
         echo '<p id=unesco>Объект Всемирного наследия ЮНЕСКО</p>';
       if ($esp_val == 1)
         echo '<p id=esp_val>Особо ценный объект</p>';
       echo '</td></tr>';
    }
    echo '<tr><th align="left">Вид объекта</th><td id=objecttype>'.$props->objectType->value."</td></tr>";
    $t="";
    foreach ($props->typologies as $x)
    {
      $t=$t."<p id=typologies>".$x->value."</p>";
    }
    echo '<tr><th align="left">Тип объекта</th><td>'.$t."</td></tr>";
    echo '<tr><th align="left">Дата создания</th><td id=date>'.$props->createDate."</td></tr>";
    if (isset($props->securityInfo))
      echo '<tr><th align="left">Описание предмета охраны</th><td id=securityinfo>'.$props->securityInfo."</td></tr>";
    if (isset($props->borderInfo))
      echo '<tr><th align="left">Описание границ объекта</th><td id=borderinfo>'.$props->borderInfo."</td></tr>";
    $t="";
    if (property_exists($props, 'documents')) foreach ($props->documents as $x)
    {
      $doctmstmp = strtotime($x->date);
      $docdate = date("d.m.Y", $doctmstmp);
      $documentid = "d".date("dmY", $doctmstmp);
      if (empty($x->archive->url) and !empty($x->archive->id)) {
        $x->archive->url = 'https://okn-mk.mkrf.ru/maps/show/id/'.$x->archive->id;
      }
      $icons = '';
      if (!empty($x->archive->id)) {
        $sth1 = $dbh->prepare('SELECT * FROM egrokn_doc_urls, files WHERE did=? AND egrokn_doc_urls.file_hash=files.file_hash');
        $sth1->execute(array($x->archive->id));
        if (($row = $sth1->fetch(PDO::FETCH_ASSOC))) {
          $icons = "<a href='https://drive.google.com/open?id=$row[google_id]'><img border=0 width=32 height=32 src='https://upload.wikimedia.org/wikipedia/commons/5/5e/Logo_of_Google_Drive_%282012-2014%29.png' alt='copy on GoogleDrive'></a>";
          $sth2 = $dbh->prepare('SELECT * FROM wvfiles WHERE file_hash=?');
          $sth2->execute(array($row['file_hash']));
          if (($row=$sth2->fetch(PDO::FETCH_ASSOC))) {
            $icons .= "<a href='https://ru.wikivoyage.org/wiki/$row[title]'><img border=0 width=32 height=32 src='https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Wikivoyage-Logo-v3-icon.svg/32px-Wikivoyage-Logo-v3-icon.svg.png' alt='copy on Wikivoyage'></a>";
          }
        }
      }
      $t=$t.'<p class=document id="'.$documentid.(empty($x->archive->url)?'':'"><a href="'.$x->archive->url).'">'.$x->name.
        (empty($x->number) ? ' ' : " № ".$x->number).
        (empty($x->date) ? '' : " от <span class=docdate>".$docdate."</span>")."</a> $icons</p>";
    }
    echo '<tr><th align="left">Документы о постановке на охрану</th><td id=documents>'.$t."</td></tr>";

    $sth = $dbh->prepare("SELECT * FROM monuments WHERE knid_new = ?");
    if ($sth->execute(array($id))) {
        while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
            $knid_arr[] = "<a href='wikivoyage.php?id=$row[knid]'>$row[knid]</a>";
        }
    }
    if ($sth->rowCount() == 0) {
        $sth1 = $dbh->prepare("SELECT * FROM dup_egrokn WHERE knid_new = ?");
        if ($sth1->execute(array($id))) {
            while ($row = $sth1->fetch(PDO::FETCH_ASSOC))
                $egrokn_arr[] = "<a href='get_info1.php?id=$row[knid_new_list]'>$row[knid_new_list]</a>";
        }
    }
  } while (0);
  echo '<tr><td align="right" colspan=2><small>Информация из <a href="http://opendata.mkrf.ru/opendata/7705851331-egrkn">Единого государственного реестра культурного наследия</a>, получена при помощи <a href="http://opendata.mkrf.ru/item/dev">API открытых данных Минкульта РФ</a>
  <br>Поддержка интерфейса: проект <a href="https://ru.wikivoyage.org/wiki/Культурное_наследие_России">Культурное наследие России</a> в Викигиде</small></td></tr>';
  empty($knid_arr) or print('<tr><th align="left">Объект(ы) Викигида</th><td>'.implode(', ', $knid_arr).'</td></tr>');
  empty($egrokn_arr) or print('<tr><th align="left">Дублирует</th><td>'.implode(', ', $egrokn_arr).'</td></tr>');
?>
  </table>
  </body>
</html>
