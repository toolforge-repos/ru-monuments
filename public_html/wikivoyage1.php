<?php
    mb_internal_encoding('utf-8');
    header("Content-type: application/json");

    $id=sprintf("%010d", $_GET["id"]);
    $row = array();

    $ts_pw = posix_getpwuid(posix_getuid());
    $ts_mycnf = parse_ini_file($ts_pw['dir'] . "/replica.my.cnf");
    $dbh = new PDO('mysql:host=tools-db;dbname=s54977__ruheritage', $ts_mycnf['user'], $ts_mycnf['password']);
    unset($ts_mycnf, $ts_pw);

    $sth = $dbh->prepare("SELECT * from monuments,pages WHERE knid = ? and monuments.pageid = pages.pageid");
    if ($sth->execute(array($id))) {
        $row = $sth->fetch(PDO::FETCH_ASSOC);
    }
    $sth->closeCursor();
    if (empty($row)) {
        if (empty($_GET['id'])) {
            // no id
        } else {
            $sth = $dbh->prepare("SELECT * from duplicates,pages WHERE knid = ? and duplicates.pageid = pages.pageid");
            if ($sth->execute(array($id))) {
                if ($sth->rowCount()>0) {
                    $row = $sth->fetch(PDO::FETCH_ASSOC);
                }
            }
            // error
        }
    }

    $data = json_decode($row['json']);
    $data->name = parse_wiki_markup($data->name);
/*    $document = '';
    if (property_exists($data, 'document')  and !empty($data->document)) {
        $document = parse_document($dbh, $data->document, $data->region);
    }
    if (property_exists($data, 'document2') and !empty($data->document2)) {
        $document .= '<br>'.parse_document($dbh, $data->document2, $data->region);
    }*/
    $data->description = parse_wiki_markup($data->description);
    echo json_encode($data, JSON_UNESCAPED_UNICODE);

/*
    $regs = array();
    foreach (file('regions.txt') as $r) {
        list($rid, $val) = explode('=', $r);
        $regs[trim($rid)] = trim($val);
    }

    $types = array(
        'architecture' => 'градостроительства и архитектуры',
        'history' => 'истории',
        'monument' => 'искусства',
        'archeology' => 'археологии',
        'settlement' => 'Историческое поселение'
    );

    $protection = array(
        '4' => 'Не охраняется государством',
        'В' => 'Выявленный объект',
        'Ф' => 'Объект федерального значения',
        'Р' => 'Объект регионального значения',
        'М' => 'Объект местного значения'
    );
    if (substr($id,2,1) == '4') $data->protection = '4';

    <tr><th class='lc'>Элемент Викиданных</th><td id=wdid>".(empty($data->wdid) ? '&nbsp;' : "<a href='https://www.wikidata.org/wiki/{$data->wdid}'>{$data->wdid}</a>")."</td></tr>
    <tr><th class='lc'>Статья в Википедии</th><td id=article>".(empty($data->wiki) ? '&nbsp;' : "<a href='https://ru.wikipedia.org/wiki/{$data->wiki}'>{$data->wiki}")."</td></tr>
    <tr><th class='lc'>Описание</th><td id=description>{$data->year} ".(empty($data->author) ? '' : ", {$data->author}. ").
        ($data->status == 'destroyed' ? ' Утраченный.' : '').
        (empty($data->dismissed) ? '' : ' Снят с охраны.').
        ' '.parse_wiki_markup($data->description)."</td></tr>
    <tr><th class='lc'>Документ</th><td id=doc>$document</td></tr>
    <tr><th class='lc'>Страница-источник</th><td id=source><a href='https://ru.wikivoyage.org/wiki/$row[title]#$id'>$row[title]</a></td></tr>");
    echo $footer;
*/

function parse_wiki_markup($str) {
    if (!empty($str)) {
        // process wiki-links [[title|description]]
        $str = preg_replace_callback(
            '!\[{2}([^|\]]*)\|?([^\]]*)\]{2}!u',
            function ($matches) {return "<a href=\"https://ru.wikivoyage.org/wiki/$matches[1]\">".(empty($matches[2]) ? $matches[1] : $matches[2]).'</a>';},
            $str);
        // process external links [url description]
        $str = preg_replace_callback(
            '!\[([hH][tT][tT][pP][sS]?://[^\][:space:]]*)\s*([^\]]*)\]!u',
            function ($matches) {return "<a href=\"$matches[1]\">".(empty($matches[2]) ? $matches[1] : $matches[2]).'</a>';},
            $str);
    }
    return $str;
}


function parse_document($dbh, $id, $region) {
    $sth = $dbh->prepare("SELECT text FROM documents WHERE id = ? and region = ?");
    $sth->execute(array($id, $region));
    if ($sth->rowCount()>0) {
        $row = $sth->fetch(PDO::FETCH_NUM);
        if (strpos($row[0], '[http') === 0) {
            list($url, $text) = explode(' ', $row[0], 2);
            $url = str_replace("'", '%27', mb_substr($url, 1));
            $text = mb_substr($text, 0, -1);
            if (parse_url($url) !== FALSE)
                $row[0] = "<a href='$url'>".htmlspecialchars($text).'</a>';
        } else if (strpos($row[0], '[[') === 0) {
            list($url, $text) = explode('|', $row[0], 2);
            $url = str_replace(' ', '_', mb_substr($url, $url{2} == ':' ? 3 : 2));
            $text = mb_substr($text, 0, -2);
            $row[0] = "<a href='https://ru.wikivoyage.org/wiki/".urlencode($url)."'>".htmlspecialchars($text).'</a>';
        }
        return $row[0];
    }
    if (!empty($region))
        return parse_document($dbh, $id, '');
    return '';
}
