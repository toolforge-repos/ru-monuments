<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title>Карточка объекта из ЕГРОКН</title>
    <style type="text/css">
      TABLE {border-collapse: collapse;}
      TH, TD {padding: 4px;}
      FORM {margin: 0;}
    </style>
  </head>
  <body>
  <table width="70%" align="center" border style="border-collapse: collapse">
    <tr><td colspan=2 valign="middle"><form action="get_info.php" method="get"><label for="id">Введите номер памятника:</label> <input id="id" name="id" value="<?php echo $_GET["id"]; ?>"> <input type="submit" value="Искать"></form></td></tr>
<?php
  $id=trim($_GET["id"]);

  $ts_pw = posix_getpwuid(posix_getuid());
  $ts_mycnf = parse_ini_file($ts_pw['dir'] . "/replica.my.cnf");
  $ts_egrokn_apikey = file_get_contents($ts_pw['dir']."/egrokn-api.key");
  $dbh = new PDO('mysql:host=tools-db;dbname=s54977__ruheritage', $ts_mycnf['user'], $ts_mycnf['password']);
  unset($ts_mycnf, $ts_pw);

  $result = FALSE;
  $url = 'https://opendata.mkrf.ru/v2/egrkn/$?f={"data.general.regNumber":{"$eq":"'.$id.'"}}';
  $opts = [
    "http" => [
        "timeout" => 10,
        "method" => "GET",
        "header" => "X-API-KEY: ".$ts_egrokn_apikey
        ]
  ];

  $context = stream_context_create($opts);
//
  $result = file_get_contents ($url, false, $context);
  $res = json_decode($result);
  do {
    if ($result === FALSE) {
      //echo '<tr><td colspan=2>Ошибка: API открытых данных Минкульта РФ в данный момент недоступен</td></tr>';
      $sth = $dbh->prepare('SELECT * FROM egrokn WHERE knid_new = ?');
      $sth->execute(array($id));
      if (!($row = $sth->fetch(PDO::FETCH_ASSOC))) {
        echo '<tr><td colspan=2>Ошибка: памятник с таким номером не найден</td></tr>';
        break;
      }
      echo "<tr><td colspan=2>Информация из копии ЕГРОКН, архивированной $row[last_update]</td></tr>";
      $res = (object)array('data'=>array(json_decode($row['json'])));
    } 
    else if ($res->status != 200 or !property_exists($res, 'total') or $res->total != 1) {
      echo '<tr><td colspan=2>Ошибка: памятник с таким номером не найден</td></tr>';
      break;
    } 

    $result = '';
    $props = $res->data[0]->data->general;
    $res = '';
    echo '<tr><th colspan=2 align="center">'.$props->name."</th></tr>";
    if (empty($props->photo->url) or is_null($props->photo->url))
        echo '<tr><td colspan=2 align="center">Нет фотографии</td></tr>';
    else
        echo '<tr><td colspan=2 align="center"><a href='.@($props->photo->url).'>Фотография</a></td></tr>';
        // (<a href='.$props->photo->preview.' type="image/jpeg">Версия-миниатюра</a>)
    echo '<tr><th align="left" width="20%">Регион расположения объекта</th><td id=region>'.$props->region->value."</td></tr>";
    $t='';
    if (isset($props->address->mapPosition->coordinates))
    {
        $lat=$props->address->mapPosition->coordinates[0];
        $long=$props->address->mapPosition->coordinates[1];
        $t="<br>(".$lat.", ".$long.")";
    }
    echo '<tr><th align="left">Адрес</th><td><span id=address>'.@($props->address->fullAddress).$t."</span></td></tr>";
    echo '<tr><th align="left">Номер в реестре</th><td id=regnumber>'.$props->regNumber."</td></tr>";
    echo '<tr><th align="left">Учётный номер</th><td id=connumber>'.$props->conNumber."</td></tr>";
    echo '<tr><th align="left">ID в наборе открытых данных</th><td id=odid>'.$props->id."</td></tr>";
    if (isset($props->parentId))
    {
      $result = false;
      $complex = false;
      $url2 = 'http://opendata.mkrf.ru/v1/egrkn/$/' . $props->parentId; 
//
      $result = file_get_contents ($url2);
      $res2 = json_decode($result);
      if ($result === false) {
        $sth2 = $dbh->prepare('SELECT * FROM egrokn WHERE nativeid = ?');
        $sth2->execute(array($props->parentId));
        if ($row = $sth2->fetch(PDO::FETCH_ASSOC)) {
          $res2 = json_decode($row['json']);
          $complex = $res2->data->general->regNumber;
          $complex_name = $res2->data->general->name;
        }
      } elseif ($res2->status == 200 & $res2->total == 1) {
        $complex = $res2->data[0]->data->general->regNumber;
        $complex_name = $res2->data[0]->data->general->name;
      }
      if ($complex !== false)
        echo '<tr><th align="left">Элемент ансамбля</th><td id=ensemble>'.$complex_name. " (<a href=get_info.php?id=$complex>". $complex ."</a>)</td></tr>";
    }
    echo '<tr><th align="left">Категория охраны</th><td id=categorytype>'.$props->categoryType->value."</td></tr>";
    $unesco=$props->unesco->id;
    $esp_val=$props->status->id;
    if ($unesco == 1 | $esp_val == 1)
    {
       echo '<tr><th align="left">Дополнительно</th><td>';
       if ($unesco == 1)
         echo '<p id=unesco>Объект Всемирного наследия ЮНЕСКО</p>';
       if ($esp_val == 1)
         echo '<p id=esp_val>Особо ценный объект</p>';
       echo '</td></tr>';
    }
    echo '<tr><th align="left">Вид объекта</th><td id=objecttype>'.$props->objectType->value."</td></tr>";
    $t="";
    if (property_exists($props,'typologies')) foreach ($props->typologies as $x)
    {
      $t=$t."<p id=typologies>".$x->value."</p>";
    }
    echo '<tr><th align="left">Тип объекта</th><td>'.$t."</td></tr>";
    echo '<tr><th align="left">Дата создания</th><td id=date>'.$props->createDate."</td></tr>";
    if (isset($props->securityInfo))
      echo '<tr><th align="left">Описание предмета охраны</th><td id=securityinfo>'.$props->securityInfo."</td></tr>";
    if (isset($props->borderInfo))
      echo '<tr><th align="left">Описание границ объекта</th><td id=borderinfo>'.$props->borderInfo."</td></tr>";
    /* Пока что не используется в реестре нормально
    if (isset($props->regimeInfo))
      echo '<tr><th align="left">Зоны охраны и режимы использования земель</th><td id=regimeinfo>'.$props->regimeInfo."</td></tr>";*/

    $t="";
    if (property_exists($props, 'documents')) foreach ($props->documents as $x)
    {
      $doctmstmp = @strtotime($x->date);
      $docdate = date("d.m.Y", $doctmstmp);
      $documentid = "d".date("dmY", $doctmstmp);
      if (empty($x->archive->url) and !empty($x->archive->id)) {
        $x->archive->url = 'https://okn-mk.mkrf.ru/maps/show/id/'.$x->archive->id;
      }
      $t=$t.'<p class=document id="'.$documentid.(empty($x->archive->url) ? '' : '"><a href="'.$x->archive->url).'">'.$x->name.
        (empty($x->number) ? ' ' : " № ".$x->number).
        (empty($x->date) ? '' : " от <span class=docdate>".$docdate."</span>")."</a></p>";
    }
    echo '<tr><th align="left">Документы о постановке на охрану</th><td id=documents>'.$t."</td></tr>";
  } while (0);
  echo '<tr><td align="right" colspan=2><small>Информация из <a href="http://opendata.mkrf.ru/opendata/7705851331-egrkn">Единого государственного реестра объектов культурного наследия</a>, получена при помощи <a href="http://opendata.mkrf.ru/item/dev">API открытых данных Минкульта РФ</a>
  <br>Поддержка интерфейса: проект <a href="https://ru.wikivoyage.org/wiki/Культурное_наследие_России">Культурное наследие России</a> в Викигиде</small></td></tr>';
?>
  </table>
  </body>
</html>
