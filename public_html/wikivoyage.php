<?php
    mb_internal_encoding('utf-8');
    echo '
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="https://tools-static.wmflabs.org/cdnjs/ajax/libs/jstree/3.3.7/themes/default/style.min.css"/>
    <script src="https://tools-static.wmflabs.org/cdnjs/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://tools-static.wmflabs.org/cdnjs/ajax/libs/jstree/3.3.7/jstree.min.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>
    <!-- links rel="stylesheet" type="text/css" href="//maps.wikimedia.org/leaflet/leaflet.css"/ -->
    <!-- script type="text/javascript" src="//maps.wikimedia.org/leaflet/leaflet.js"></script -->
    <style type="text/css">
      TABLE {border-collapse: collapse;}
      TH, TD {padding: 4px;}
      .lc {text-align: left; width: 30%;}
      FORM {margin: 0;}
      #mapid {height: 180px;}
    </style>
    <title>Объект культурного наследия</title>
  </head>
  <body>
    <table width="55%" align="center" border>';
    $id=sprintf("%010d", $_GET["id"]);
    echo '<tr><td colspan="2" valign="middle"><form method="get">Введите номер памятника: <input id="id" name="id" value="'.$id.'"> <input type="submit" value="Искать"></form></td></tr>';

    $footer = '<tr><td align="right" colspan=2><small>Информация из проекта <a href="https://ru.wikivoyage.org/wiki/%D0%9A%D1%83%D0%BB%D1%8C%D1%82%D1%83%D1%80%D0%BD%D0%BE%D0%B5_%D0%BD%D0%B0%D1%81%D0%BB%D0%B5%D0%B4%D0%B8%D0%B5_%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D0%B8">Культурное наследие России</a> в Викигиде</small></td></tr></table></body></html>';
    $row = array();

    $ts_pw = posix_getpwuid(posix_getuid());
    $ts_mycnf = parse_ini_file($ts_pw['dir'] . "/replica.my.cnf");
    set_exception_handler(function ($ex) {
        echo "Увы! SQL-сервер прилёг отдохнуть. Повторите запрос позже...";
        die;
    });

    $dbh = new PDO('mysql:host=tools-db;dbname=s54977__ruheritage', $ts_mycnf['user'], $ts_mycnf['password']);
    unset($ts_mycnf, $ts_pw);

    restore_exception_handler();

    $sth = $dbh->prepare("SELECT * from monuments,pages WHERE knid = ? and monuments.pageid = pages.pageid");
    if ($sth->execute(array($id))) {
        $row = $sth->fetch(PDO::FETCH_ASSOC);
    }
    $sth->closeCursor();
    if (empty($row)) {
        if (empty($_GET['id'])) {
            echo '<tr><td colspan="2"><small>Укажите в поле выше 10-значный номер объекта культурного наследия.</small></td></tr>'.$footer;
        } else {
            $sth = $dbh->prepare("SELECT * from duplicates,pages WHERE knid = ? and duplicates.pageid = pages.pageid");
            if ($sth->execute(array($id))) {
                if ($sth->rowCount()>0) {
                    $row = $sth->fetch(PDO::FETCH_ASSOC);
                    if ($row['knid_list'] != 0) {
                        echo "
    <tr><td colspan=2>Элемент дублирует запись с номером <a href='$_SERVER[PHP_SELF]?id=$row[knid_list]'>$row[knid_list]</a></tr>
    <tr><th align='left' width='30%'>Страница-источник</th><td id=source><a href='https://ru.wikivoyage.org/wiki/$row[title]#$id'>$row[title]</a></td></tr>";
                    } else {
                        echo "
    <tr><td colspan=2>Элемент помечен как ошибочный.</tr>
    <tr><th align='left' width='30%'>Страница-источник</th><td id=source><a href='https://ru.wikivoyage.org/wiki/$row[title]#$id'>$row[title]</a></td></tr>";
                    }
                    echo $footer;
                    exit;
                }
            }
            echo '<tr><td colspan="2">Ошибка: памятник с таким номером не найден</td></tr>'.$footer;
        }
        exit;
    }

    $data = json_decode($row['json']);
    $thumb = false;

    if (!empty($data->image)) {
//        $image_source = array('https://commons.wikimedia.org', 'https://ru.wikivoyage.org');
        $image_source = array('https://ru.wikivoyage.org', 'https://commons.wikimedia.org');
        foreach ($image_source as $img_base_url) {
            $img_info = file_get_contents($img_base_url.'/w/api.php?action=query&format=json&prop=imageinfo&titles=File:'.urlencode($data->image).'&iiprop=url&iiurlwidth=320&iiurlheight=240');
            $img_info = json_decode($img_info);
            $thumb = reset($img_info->query->pages);
            if (property_exists($thumb, 'imageinfo')) {
                $thumb = $thumb->imageinfo[0]->thumburl;
            } else {
                $thumb = false;
            }
            if (!empty($thumb)) break;
        }
    }

    $regs = array();
    foreach (file('regions.txt') as $r) {
        list($rid, $val) = explode('=', $r);
        $regs[trim($rid)] = trim($val);
    }

    $types = array(
        'architecture' => 'градостроительства и архитектуры',
        'history' => 'истории',
        'monument' => 'искусства',
        'archeology' => 'археологии',
        'settlement' => 'Историческое поселение'
    );

    $protection = array(
        'Н' => 'Не охраняется государством',
        'Ц' => 'Исторически ценный градоформирующий (средоформирующий) объект',
        'В' => 'Выявленный объект',
        'Ф' => 'Объект федерального значения',
        'Р' => 'Объект регионального значения',
        'М' => 'Объект местного значения'
    );
    if (substr($id,2,1) == '4' and empty($data->protection)) $data->protection = 'Н';

    echo "
    <tr><th colspan=2 align='center'>".parse_wiki_markup($data->name)."</th></tr>
    <tr><td colspan=2 align='center'>".(empty($data->image) || empty($thumb) ? '' : "<a href='$img_base_url/wiki/File:".rawurlencode($data->image)."'><img src='$thumb'></a><br>").
        (!empty($data->commonscat) ? "<a href='https://commons.wikimedia.org/wiki/Category:".rawurlencode($data->commonscat)."'>Категория Commons</a><br>" : '').
        "<a href='https://commons.wikimedia.org/wiki/Category:WLM/$id'>Галерея</a></td></tr>
    <tr><th class='lc'>Номер в списках</th><td id=knid>{$data->knid}</td></tr>";
    if (property_exists($data, 'complex') and !empty($data->complex)) {
        $sth = $dbh->prepare("SELECT knid,json from monuments WHERE complex = ? order by knid");
        $complex_string = '';
        $complex_main = false;
        if ($sth->execute(array($data->complex))) {
            while ($elem = $sth->fetch(PDO::FETCH_ASSOC)) {
                $elem_data = json_decode($elem['json']);
                if ($elem['knid'] == $data->complex) {
                    $complex_main = $elem_data;
                    continue;
                }
                $_tag = ($elem['knid'] == $id ? 'strong' : 'span');
                $complex_string .= "<li id=$elem[knid]".($elem['knid'] == $id ? ' data-jstree=\'{"selected": true}\'' : '').
                "/><$_tag>$elem[knid] &mdash; ".(mb_strlen($elem_data->name) > 25 ? mb_substr($elem_data->name,0,25).'...' : $elem_data->name)."</$_tag>";
            }
            echo "
    <tr><th class='lc' valign='top'>Комплекс</th><td id=complex>".@($complex_main->name)."
    <div id=clist><ul><li id={$data->complex}".
            ($data->complex == $id ? ' data-jstree=\'{"selected": true}\'/><strong>'.$data->complex.'</strong>' : "/>{$data->complex}").':<ul>'.
            $complex_string."
    </ul></ul></div></td></tr>
    <script type='text/javascript'><!--
        $(function () {
            $('#clist').jstree({
                'core' : { 'expand_selected_onload' : false },
                'plugins' : [ ]
            });
            $('#clist').on('changed.jstree', function (e, data) {
                if (data.node.id == '$id') return false;
                window.location='$_SERVER[PHP_SELF]?id='+data.node.id;
            });
        });
    //--></script>";
        }
        $sth->closeCursor();
    }
    $document = '';
    if (property_exists($data, 'document')  and !empty($data->document)) {
        $document = parse_document($dbh, $data->document, $data->region);
    }
    if (property_exists($data, 'document2') and !empty($data->document2)) {
        $document .= (empty($document) ? '' : '<br>').parse_document($dbh, $data->document2, $data->region);
    }
    if (property_exists($data, 'dismissed') and !empty($data->dismissed)) {
        $document .= (empty($document) ? '' : '<br>').parse_document($dbh, $data->dismissed, $data->region);
    }
    
    $pass_info = file_get_contents('https://ru.wikivoyage.org/w/api.php?action=query&format=json&prop=imageinfo&titles='.rawurlencode('Файл:Passport '.$data->knid.'.pdf'));
    $pass_info = json_decode($pass_info);
    $passport = reset($pass_info->query->pages);
    if (property_exists($passport, 'pageid') and $passport->pageid>0) {
        $document .= (empty($document) ? '' : '<br>')."<a href='https://ru.wikivoyage.org/wiki/Файл:Passport {$data->knid}.pdf'>Паспорт ОКН</a>";
    }

    echo @("
    <tr><th class='lc'>Номер <a href='https://opendata.mkrf.ru/opendata/7705851331-egrkn'>ЕГРОКН</a></th><td id=knid_new><a href='get_info.php?id={$data->knid_new}'>{$data->knid_new}</a></td></tr>
    <tr><th class='lc'>Типология</th><td id=type>".($data->type == 'settlement' ? '' : (substr($id,2,1) == '4' ? 'Объект с признаками памятника ' : 'Памятник '))."{$types[$data->type]}</td></tr>
    <tr><th class='lc'>Категория охраны</th><td id=protection>{$protection[$data->protection]}</td></tr>
    <tr><th class='lc'>Регион</th><td id=region>{$regs[$data->region]}</td></tr>
    <tr><th class='lc'>Район</th><td id=district>{$data->district}</td></tr>
    <tr><th class='lc'>Населённый пункт</th><td id=city>".(!empty($data->munid) ? "<a href='https://www.wikidata.org/wiki/{$data->munid}'>{$data->municipality}</a>" : $data->municipality)."</td></tr>
    <tr><th class='lc'>Адрес</th><td id=address>".(property_exists($data, 'block') && !empty($data->block) ? "квартал {$data->block}, " : '')."{$data->address}</td></tr>
    <tr><th class='lc'>Координаты</th><td id=coords>".(empty($data->lat) || empty($data->long) ? '&nbsp;' : 
        "<div style='margin:2pt;'><a href='https://tools.wmflabs.org/geohack/geohack.php?pagename=".str_replace('/',':',$row['title']).":$id&params={$data->lat}_N_{$data->long}_E_globe:earth&language=ru' title='Карты и инструменты GeoHack'>{$data->lat}, {$data->long}</a>
        <sup><small><a href='https://maps.yandex.ru/?ll={$data->long},{$data->lat}&spn=0.02,0.02&pt={$data->long},{$data->lat}&l=map' title='Это место на Яндекс.Картах'>(Я)</a></small></sup>
        <sup><small><a href='https://maps.google.com/maps?ll={$data->lat},{$data->long}&spn=0.02,0.02&t=m&q={$data->lat},{$data->long}' title='Это место на картах Google'>(G)</a></small></sup>
        <sup><small><a href='https://www.openstreetmap.org/?mlat={$data->lat}&mlon={$data->long}&zoom=13&layers=M' title='Это место на карте OpenStreetMap'>(O)</a></small></sup></div>
        <div id='mapid'></div>
        <script type='text/javascript'>
            var wvmap = L.map('mapid').setView([{$data->lat}, {$data->long}], 13);
            var tile = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
//            var tile = 'https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png';
            L.tileLayer(tile, {attribution: '&copy; <a href=\"https://www.openstreetmap.org/\">OpenStreetMap</a> contributors, <a href=\"https://creativecommons.org/licenses/by-sa/2.0/\">CC-BY-SA</a>'}).addTo(wvmap);
            var marker = L.marker([{$data->lat}, {$data->long}]).addTo(wvmap);
        </script>")."</td></tr>
    <tr><th class='lc'>Элемент Викиданных</th><td id=wdid>".(empty($data->wdid) ? '&nbsp;' : "<a href='https://www.wikidata.org/wiki/{$data->wdid}'>{$data->wdid}</a>")."</td></tr>
    <tr><th class='lc'>Статья в Википедии</th><td id=article>".(empty($data->wiki) ? '&nbsp;' : "<a href='https://ru.wikipedia.org/wiki/{$data->wiki}'>{$data->wiki}")."</td></tr>
    <tr><th class='lc'>Описание</th><td id=description>{$data->year} ".(empty($data->author) ? '' : ", {$data->author}. ").
        ($data->status == 'destroyed' ? ' Утраченный.' : '').
        (empty($data->dismissed) ? '' : ' Снят с охраны.').
        ' '.parse_wiki_markup($data->description)."</td></tr>
    <tr><th class='lc'>Документ</th><td id=doc>$document</td></tr>
    <tr><th class='lc'>Страница-источник</th><td id=source><a href='https://ru.wikivoyage.org/wiki/$row[title]#$id'>$row[title]</a></td></tr>");
    echo $footer;

function parse_wiki_markup($str) {
    if (!empty($str)) {
        // process wiki-links [[title|description]]
        $str = preg_replace_callback(
            '!\[{2}([^|\]]*)\|?([^\]]*)\]{2}!u',
            function ($matches) {return "<a href=\"https://ru.wikivoyage.org/wiki/$matches[1]\">".(empty($matches[2]) ? $matches[1] : $matches[2]).'</a>';},
            $str);
        // process external links [url description]
        $str = preg_replace_callback(
            '!\[([hH][tT][tT][pP][sS]?://[^\][:space:]]*)\s*([^\]]*)\]!u',
            function ($matches) {return "<a href=\"$matches[1]\">".(empty($matches[2]) ? $matches[1] : $matches[2]).'</a>';},
            $str);
    }
    return $str;
}


function parse_document($dbh, $id, $region) {
    $sth = $dbh->prepare("SELECT text FROM documents WHERE id = ? and region = ?");
    $sth->execute(array($id, $region));
    if ($sth->rowCount()>0) {
        $row = $sth->fetch(PDO::FETCH_NUM);
        if (strpos($row[0], '[http') === 0) {
            list($url, $text) = explode(' ', $row[0], 2);
            $url = str_replace("'", '%27', mb_substr($url, 1));
            $text = mb_substr($text, 0, -1);
            if (parse_url($url) !== FALSE)
                $row[0] = "<a href='$url'>".htmlspecialchars($text).'</a>';
        } else if (strpos($row[0], '[[') === 0) {
            list($url, $text) = explode('|', $row[0], 2);
            $url = str_replace(' ', '_', mb_substr($url, $url{2} == ':' ? 3 : 2));
            $text = mb_substr($text, 0, -2);
            $row[0] = "<a href='https://ru.wikivoyage.org/wiki/".urlencode($url)."'>".htmlspecialchars($text).'</a>';
        }
        return $row[0];
    }
    if (!empty($region))
        return parse_document($dbh, $id, '');
    return '';
}
